import socket
import string
import struct
import re
import argparse


class Waker:
    def __init__(self) -> None:
        self.__mac = None
        self.__macHEX = None
        self.__package = None

    def parseMac(self):
        if re.match(
            "[0-9a-f]{2}([-:]?)[0-9a-f]{2}(\\1[0-9a-f]{2}){4}$", self.__mac.lower()
        ):
            self.__mac = self.__mac.replace("-", ":")
            splitMac = str.split(self.__mac, ":")
            self.__macHEX = struct.pack(
                "BBBBBB",
                int(splitMac[0], 16),
                int(splitMac[1], 16),
                int(splitMac[2], 16),
                int(splitMac[3], 16),
                int(splitMac[4], 16),
                int(splitMac[5], 16),
            )
        else:
            assert False & "No valid MAC"

    def makeMagicPacket(self):
        # create the magic packet from  HEXMAC address
        self.__package = b"\xff" * 6 + self.__macHEX * 16

    def sendMagicPacket(self, macAddress, destIP, destPort=7):
        print(macAddress)#
        print(destIP)
        self.__mac = macAddress
        self.parseMac()
        self.makeMagicPacket()
        # Create the socket connection and send the packet
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.sendto(self.__package, (destIP, destPort))
        s.close()
        print("Packet successfully sent to " + self.__mac)
        pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Sends and Magic Package to Wake up devices over LAN')
    parser.add_argument('-m', '--mac', help='Hardware adress of device', required=True)
    parser.add_argument('-i', '--ip', help='The IP address where the packet should be sent', required=True)
    parser.add_argument('-p', '--port', type=int, help='The IP address where the packet should be sent', required=True)
    args = parser.parse_args()
    print(args)

    wol = Waker()
    wol.sendMagicPacket(args.mac, args.ip, args.port)
    
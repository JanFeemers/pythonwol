# PythonWOL

Python script to wake up others PC over LAN

## Purpose

For saving energy we shedule shutdowns for my NAS.

Unfortunally Samab-Shares can not do WOL.

## Usage

You need to add an entry in your arp table.

```shell
python main.py -m MAC -i IP -p PORT
```

### Windows

You can do this on windows running this batch file:

```batch
@setlocal enableextensions
@cd /d "%~dp0"

netsh interface ip add neighbors Drahtlosnetzwerkverbindung IP MAC & 
netsh interface ip add neighbors LAN-Verbindung IP MAC & 

py main.py -m "MAC" -i "IP" -p 7

set /p DUMMY=Hit ENTER to continue...

```
